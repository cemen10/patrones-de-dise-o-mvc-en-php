<?php

/*
 * Base PHP para un proyecto MVC usando un diseño de base de datos
 * orientadas a objetos (con Mapeo Objeto Relacional | ORM) 
 * @autor ING. CARLOS MENDOZA
 * 
 */

require './config.php';

//Condicional Avanzado
// var = () ? Verdader:Falso
$url = ( isset($_GET["url"]) ) ? $_GET["url"] : "Index/index";
$url = explode("/", $url);
$controller = ( isset($url[0]) ) ? $url[0] . "_controller" : "Index_controller";
$method = ( $url[1] != null ) ? $url[1] : "index";
$params = ( isset($url[2]) && $url[2] != null ) ? $url[2] : null;
//echo "Controlador: ".$controller;
//echo "<br> Metodo: ".$method;
//echo "<br> Parametro: ".$params;

spl_autoload_register(function($class) {
    if (file_exists(LIBS . $class . '.php')) {
        require LIBS . $class . '.php';
    }elseif(MODELS . $class . '.php'){
        require MODELS . $class . '.php';
    }else{
        if(BS . $class . '.php'){
            require BS . $class . '.php';
        }else {
            exit("La clase: ".$class." no ha sido definida");
        }
    }
});

$path = "./controllers/" . $controller . ".php";

if (file_exists($path)) {
    require $path;
    $controller = new $controller();

    if (method_exists($controller, $method)) {
        if ($params != null) {
            $controller->{$method}($params);
        } else {
            $controller->{$method}();
        }
    } else {
        exit("Invalid Method");
    }
} else {
    exit("Invalid Controller");
}