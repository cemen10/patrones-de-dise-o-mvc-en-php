<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author ING. CARLOS MENDOZA
 */
class View {

    public function render($controller, $view, $title = '') {
        //obtener la clase de ese controlador
        //$controler=Index_controller
        $controller = get_class($controller);
        //quitar _controller
        $controller = substr($controller, 0, -11);
        //ruta de la vista
        $path = './views/' . $controller . '/' . $view;
        if (file_exists($path . ".php")) {
            if ($title != '') {
                $this->title = $title;
            }
            require $path . '.php';
        } elseif (file_exists($path . ".html")) {
            if ($title != '') {
                $this->title = $title;
            }
            require $path . '.html';
        } else {
            echo "Error: Invalid View " . $view . " to render";
        }
    }

}
