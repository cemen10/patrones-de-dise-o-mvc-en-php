<?php
    define('URL','http://localhost:8080/MVCMEJOR/');
    define('LIBS','libs/');
    define('MODELS','./models/');
    define('BS','./bussinesLogic/');
    define('MODULES','./views/modules/');
        
    define('_DB_TYPE', 'mysql');
    define('_DB_HOST' , 'localhost');
    define('_DB_USER' , 'root' );
    define('_DB_PASS' , '' );
    define('_DB_NAME' , 'myshop');
    
    define('HASH_ALGO','sha512');
    define('HASH_KEY','my_key');
    define('HASH_WORD','my_secret');
    define('SECRET_WORD','so_secret');