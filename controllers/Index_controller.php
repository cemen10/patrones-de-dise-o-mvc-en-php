<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author ING. CARLOS MENDOZA
 */
class Index_controller extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function index($param = null) {
        if ($param == null) {
            $param = "tu";
        }
        echo "Hola " . $param . "!";
    }

    public function crearUsuario() {
        if(isset($_GET['username'])&& isset($_GET['password'])){
            $username=$_GET['username'];
            $password=$_GET['password'];
            
            //crear una instancia de usuario 2 formas
            // 1. TRADICIONAL
            $usuario=new Usuario(null, $username, $password);
            $usuario->create();
            // 2. OTRA FORMA ---instanciate se encuentra en el Model.php
            //$args=["username"=>$username,"password"=>$password];
            //$usuario=  Usuario::instanciate($args);
            //$usuario->create();
            
            print_r($usuario);
            echo "</br> Todos Los Usuarios </br>";
            print_r(Usuario::getAll()) ;
        }
    }
    
    public function buscar($username){
        if(!empty($username)){
            $r=  Usuario::getBy("username", $username);
            if(is_null($r)){
                echo 'No Existe ';
            }else{
                print_r($r);
            }
        }
    }

}
